﻿using Microsoft.EntityFrameworkCore;
using my_books.Data.Models;
using my_books.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace my_books.Data.Services
{
    public class BooksService
    {
        private AppDbContext _context;
        public BooksService(AppDbContext context)
        {
            _context = context;
        }

        public void AddBookWithAuthors(BookVM book)
        {
            var _book = new Book()
            {
                CoverUrl = book.CoverUrl,
                DateAdded = DateTime.Now,
                DateRead = book.IsRead ? book.DateRead.Value : null,
                Description = book.Description,
                Genre = book.Genre,
                IsRead = book.IsRead,
                Rate = book.IsRead ? book.Rate.Value : null,
                Title = book.Title,
                PublisherId = book.PublisherId
            };
            _context.Books.Add(_book);
            _context.SaveChanges();

            foreach( var id in book.AuthorIds)
            {
                var _book_Author = new Book_Author()
                {
                    BookId = _book.Id,
                    AuthorId = id
                };
                _context.Book_Authors.Add(_book_Author);
                _context.SaveChanges();
            }
        }
    
        public List<Book> GetAllBooks() =>_context.Books.ToList();
        public BookWithAuthorsVM GetBookById(int bookId)
        {
            var _bookWithAuthors = _context.Books.Where(n=>n.Id==bookId).Select(book=>new BookWithAuthorsVM() {
                CoverUrl = book.CoverUrl,
                DateRead = book.IsRead ? book.DateRead.Value : null,
                Description = book.Description,
                Genre = book.Genre,
                IsRead = book.IsRead,
                Rate = book.IsRead ? book.Rate.Value : null,
                Title = book.Title,
                PublisherName = book.Publisher.Name,
                AuthorNames = book.Book_Authors.Select(n=> n.Author.FullName).ToList()
            }).FirstOrDefault();
            return _bookWithAuthors;
        }

        public Book UpdateBookById(int bookId, BookVM book)
        {
            var _book = _context.Books.FirstOrDefault(b => b.Id == bookId);
            Console.WriteLine(_book.DateAdded.ToString());
            if(_book is not null)
            {
                _book.CoverUrl = book.CoverUrl;
                _book.DateAdded = DateTime.Now;
                _book.DateRead = book.IsRead ? book.DateRead.Value : null;
                _book.Description = book.Description;
                _book.Genre = book.Genre;
                _book.IsRead = book.IsRead;
                _book.Rate = book.IsRead ? book.Rate.Value : null;
                _book.Title = book.Title;

                _context.Entry(_book).State = EntityState.Modified;
                _context.Entry(_book).Property(b => b.DateAdded).IsModified = false;
                _context.SaveChanges();
            }
            return _book;
        }

        public void DeleteBookById(int bookId)
        {
            var _book = _context.Books.FirstOrDefault(b => b.Id == bookId);
            _context.Books.Remove(_book);
            _context.SaveChanges();
        }


    }
}
